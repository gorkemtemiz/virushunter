﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AnimationControl : MonoBehaviour
{
    public void LoadNewScene()
    {
        StartCoroutine(LoadNewSceneCoroutine());
    }

    IEnumerator LoadNewSceneCoroutine()
    {
        Scene scene = SceneManager.GetActiveScene();
        yield return new WaitForSeconds(0.225f);
        SceneManager.LoadScene(scene.buildIndex + 1);
    }
}
