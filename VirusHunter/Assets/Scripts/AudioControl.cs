﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class AudioControl : MonoBehaviour
{
    // This script is attached to AudioManager object in Today_Info Scene
    public AudioSource mainThemeAudio;
    public AudioSource gameStartAudio;
    public AudioSource busEnvironmentAudio;
    public AudioSource parkEnvironmentAudio;
    public AudioSource toiletEnvironmentAudio;
    public AudioSource celebrationCheerAudio;
    public AudioSource sprayEffectAudio;

    private Scene currentScene;
    private AudioSource currentAS;
    private float fadeTime = 1;


    public static AudioControl Instance;
    void Awake()
    {
        if (Instance != null)
        {
            GameObject.Destroy(Instance);
        }
        else
        {
            Instance = this;
            DontDestroyOnLoad(this);
        }

        mainThemeAudio.volume = 0.7f;
        sprayEffectAudio.volume = 1f;
        
        StartCoroutine(GameIntroSFXCoroutine());
    }

    private void Update()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        Debug.Log("event worked!");
        currentScene = SceneManager.GetActiveScene();

        switch (currentScene.buildIndex)
        {
            case 1:
                StopAllSources();
                mainThemeAudio.volume = 1f;
                break;
            case 2:
                StartCoroutine(BeforeGameSceneCoroutine("bus"));
                break;
            case 3:
                StartCoroutine(StartGameSceneCoroutine("bus"));
                break;
            case 4:
                StopAllSources();
                mainThemeAudio.volume = 1f; 
                mainThemeAudio.Play();
                break;
            case 5:
                StartCoroutine(BeforeGameSceneCoroutine("park"));
                break;
            case 6:
                StartCoroutine(StartGameSceneCoroutine("park"));
                break;
            case 7:
                StopAllSources();
                mainThemeAudio.volume = 1f;
                mainThemeAudio.Play();
                break;
            case 8:
                StartCoroutine(BeforeGameSceneCoroutine("toilet"));
                break;
            case 9:
                StartCoroutine(StartGameSceneCoroutine("toilet"));
                break;
            case 10:
                StopAllSources();
                mainThemeAudio.volume = 1f;
                mainThemeAudio.Play();
                break;
            case 11:
                StopAllSources();
                mainThemeAudio.volume = 1f;
                break;
            case 12:
                StopAllSources();
                mainThemeAudio.volume = 1f;
                break;
        }
    }

    IEnumerator GameIntroSFXCoroutine()
    {
        yield return new WaitForSeconds(3f);
        sprayEffectAudio.PlayOneShot(sprayEffectAudio.clip);
        yield return new WaitForSeconds(sprayEffectAudio.clip.length + 0.25f);
        sprayEffectAudio.PlayOneShot(sprayEffectAudio.clip);
        yield return new WaitForSeconds(sprayEffectAudio.clip.length + 0.25f);
        sprayEffectAudio.PlayOneShot(sprayEffectAudio.clip);
    }

    IEnumerator BeforeGameSceneCoroutine(string environment)
    {
        StopAllSources();
        mainThemeAudio.volume = 1f;

        switch (environment)
        {
            case "bus":
                busEnvironmentAudio.volume = 0.5f;
                busEnvironmentAudio.Play();
                currentAS = busEnvironmentAudio;
                break;
            case "park":
                parkEnvironmentAudio.volume = 0.5f;
                parkEnvironmentAudio.Play();
                currentAS = parkEnvironmentAudio;
                break;
            case "toilet":
                toiletEnvironmentAudio.volume = 0.2f;
                toiletEnvironmentAudio.Play();
                currentAS = toiletEnvironmentAudio;
                break;
        }
        yield return null;
    }

    IEnumerator StartGameSceneCoroutine(string environment)
    {
        StopAllSources();
        mainThemeAudio.volume = 0.5f;
        gameStartAudio.volume = 1f;
        gameStartAudio.Play();
        yield return new WaitForSeconds(gameStartAudio.clip.length);

        switch (environment)
        {
            case "bus":
                busEnvironmentAudio.volume = 1f;
                currentAS = busEnvironmentAudio;
                busEnvironmentAudio.Play();
                break;
            case "park":
                parkEnvironmentAudio.volume = 1f;
                currentAS = parkEnvironmentAudio;
                parkEnvironmentAudio.Play();
                break;
            case "toilet":
                toiletEnvironmentAudio.volume = 0.4f;
                currentAS = toiletEnvironmentAudio;
                toiletEnvironmentAudio.Play();
                break;
        }

        yield return new WaitForSeconds(10f);

        float t = fadeTime;
        while (t > 0)
        {
            yield return null;
            t -= 0.1f * Time.deltaTime;
            currentAS.volume = t / fadeTime;
            mainThemeAudio.volume += 0.5f * (1 - t / fadeTime);
        }
        StopAllSources();
    }

    public void StopAllSources()
    {
        gameStartAudio.Stop();
        busEnvironmentAudio.Stop();
        parkEnvironmentAudio.Stop();
        toiletEnvironmentAudio.Stop();
        celebrationCheerAudio.Stop();
        sprayEffectAudio.Stop();
    }
}
