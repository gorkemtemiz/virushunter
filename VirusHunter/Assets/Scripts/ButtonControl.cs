﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonControl : MonoBehaviour
{
    [Header("User Interface Objects")]
    public GameObject gameEnvironment;
    public GameObject inGamePanel;
    public GameObject pausePanel;
    public GameObject infoPanel;

    public AudioSource buttonClickAudio;

    public void LoadNextScene()
    {
        StartCoroutine(LoadNextSceneCoroutine());
    }

    public void PauseTheGame()
    {
        StartCoroutine(PauseTheGameCoroutine());
    }
    public void BackToGame()
    {
        StartCoroutine(BackToGameCoroutine());    
    }

    public void RestartTheGame()
    {
        StartCoroutine(RestartTheGameCoroutine());
    }

    public void ShowInfo()
    {
        StartCoroutine(ShowInfoCoroutine());
    }

    public void BackToPausePanel()
    {
        StartCoroutine(BackToPausePanelCoroutine());
        
    }

    IEnumerator LoadNextSceneCoroutine()
    {
        buttonClickAudio.Play();
        yield return new WaitForSeconds(buttonClickAudio.clip.length);
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.buildIndex + 1);
    }

    IEnumerator PauseTheGameCoroutine()
    {
        buttonClickAudio.Play();
        yield return new WaitForSeconds(buttonClickAudio.clip.length);
        inGamePanel.SetActive(false);
        gameEnvironment.SetActive(false);
        pausePanel.SetActive(true);
    }

    IEnumerator BackToGameCoroutine()
    {
        buttonClickAudio.Play();
        yield return new WaitForSeconds(buttonClickAudio.clip.length);
        pausePanel.SetActive(false);
        gameEnvironment.SetActive(true);
        inGamePanel.SetActive(true);
    }

    IEnumerator RestartTheGameCoroutine()
    {
        buttonClickAudio.Play();
        yield return new WaitForSeconds(buttonClickAudio.clip.length);
        SceneManager.LoadScene(2);
    }

    IEnumerator ShowInfoCoroutine()
    {
        buttonClickAudio.Play();
        yield return new WaitForSeconds(buttonClickAudio.clip.length);
        pausePanel.SetActive(false);
        infoPanel.SetActive(true);
    }

    IEnumerator BackToPausePanelCoroutine()
    {
        buttonClickAudio.Play();
        yield return new WaitForSeconds(buttonClickAudio.clip.length);
        infoPanel.SetActive(false);
        pausePanel.SetActive(true);
    }
}
