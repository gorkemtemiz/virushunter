﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameControl : MonoBehaviour
{
    [Header("Sprite Arrays")]
    public Sprite[] germSprites;
    public Sprite[] backgroundSprites;


    [Header("Game Objects")]
    public GameObject GermsParent;
    public Slider hygieneMeter;
    public Slider GermMeter;
    public GameObject Gadget;
    public Animator gadgetAnimator;


    [Header("Game Parameters")]
    public float gadgetDeltaPosY;

    //--------------------------- private area------------------------------
    private Vector3 screenPoint;
    private Vector2 mousePos;
    private Vector3 hitPoint;
    [HideInInspector] public AudioSource sprayAudioSource;
    //--------------------------- private area------------------------------


    void Start()
    {
        SpriteRenderer[] germsSpriteArray = GermsParent.transform.GetComponentsInChildren<SpriteRenderer>();
        for(int i = 0; i < germsSpriteArray.Length; i++)
        {
            int randomSpriteIndex = Random.Range(0, germSprites.Length);
            germsSpriteArray[i].sprite = germSprites[randomSpriteIndex];
        }
        sprayAudioSource = Gadget.GetComponent<AudioSource>();
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            screenPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mousePos = new Vector2(screenPoint.x, screenPoint.y);
            RaycastHit2D hit = Physics2D.Raycast(mousePos, Vector2.zero, 100f);

            if(hit.collider != null)
            {
                hitPoint = new Vector3(hit.transform.position.x, hit.transform.position.y - gadgetDeltaPosY, hit.transform.position.z);

                Gadget.SetActive(true);
                gadgetAnimator.transform.position = hitPoint;
                gadgetAnimator.Play(Gadget.gameObject.name + "Anim", -1, 0);
                Gadget.transform.GetChild(0).GetComponent<ParticleSystem>().Play();
                sprayAudioSource.Play(0);


                StartCoroutine("WaitUntilProcessEnd", sprayAudioSource.clip.length);
                hit.collider.gameObject.SetActive(false);
                print("the germ at " + Input.mousePosition + " is destroyed!");
                IncreaseTheHygieneProgress(1);
                DecreaseTheGermProgress(1);
            }
        }
    }

    private void IncreaseTheHygieneProgress(int newProgress)
    {
        hygieneMeter.value += newProgress;
    }
    private void DecreaseTheGermProgress(int newProgress)
    {
        GermMeter.value -= newProgress;
        if(GermMeter.value == 0)
        {
            GermMeter.gameObject.transform.GetChild(1).GetChild(0).gameObject.SetActive(false);
        }
    }


    IEnumerator WaitUntilProcessEnd(float sec)
    {
        yield return new WaitForSeconds(sec);
        Gadget.transform.position = new Vector3(100, 100, 0);
    }
}
