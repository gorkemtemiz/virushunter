﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class GermRotationControl : MonoBehaviour
{
    private void Start()
    {
        StartCoroutine(DoSwing());
    }

    private void OnEnable()
    {
        StartCoroutine(DoSwing());
    }

    IEnumerator DoSwing()
    {
        while (gameObject.activeInHierarchy)
        {
            gameObject.transform.DOLocalRotate(new Vector3(0, 0, Random.Range(-60,60)), 1.5f);
            yield return new WaitForSeconds(1);
        }

    }

}
