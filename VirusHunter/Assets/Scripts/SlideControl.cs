﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SlideControl : MonoBehaviour
{
    public GameObject[] slidePages;
    public AudioSource buttonClickAudioSource;
    public GameObject previousPageButton;


    private int slideIndex;
    private bool isClicked;

    private void Start()
    {
        slideIndex = 0;
        isClicked = false;
    }

    private void Update()
    {
        
        if (slideIndex == 1)
        {
            previousPageButton.SetActive(true);
        }

        if (slideIndex == 0)
        {
            previousPageButton.SetActive(false);
        }
    }

    public void GoNextSlidePage()
    {
        StartCoroutine(GoNextSlideCoroutine());
    }

    public void GoPreviousSlidePage()
    {
        StartCoroutine(GoPreviousSlideCoroutine());
    }

    IEnumerator GoNextSlideCoroutine()
    {
        if (!isClicked)
        {
            isClicked = true;

            if (slideIndex + 1 < slidePages.Length)
            {
                buttonClickAudioSource.Play();
                yield return new WaitForSeconds(buttonClickAudioSource.clip.length);
                slidePages[slideIndex].SetActive(false);
                slidePages[slideIndex + 1].SetActive(true);
                slideIndex++;
            }
            else if (slideIndex + 1 == slidePages.Length)
            {
                buttonClickAudioSource.Play();
                yield return new WaitForSeconds(buttonClickAudioSource.clip.length);
                Scene scene = SceneManager.GetActiveScene();
                SceneManager.LoadScene(scene.buildIndex + 1);
            }

            isClicked = false;
        }
        else
            yield return null;
    }

    IEnumerator GoPreviousSlideCoroutine()
    {
        if (!isClicked)
        {
            isClicked = true;

            if (slideIndex - 1 >= 0)
            {
                buttonClickAudioSource.Play();
                yield return new WaitForSeconds(buttonClickAudioSource.clip.length);
                slidePages[slideIndex].SetActive(false);
                slidePages[slideIndex - 1].SetActive(true);
                slideIndex--;
            }
            else if (slideIndex - 1 < 0)
            {
                buttonClickAudioSource.Play();
                yield return new WaitForSeconds(buttonClickAudioSource.clip.length);
            }

            isClicked = false;
        }
        else
            yield return null;
    }
}
