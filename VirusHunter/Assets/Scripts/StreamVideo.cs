﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;
using System.IO;

public class StreamVideo : MonoBehaviour
{
    public string videoNameWithFormat;
    public RawImage rawImage;
    public VideoPlayer videoPlayer;
    public AudioSource audioSource;
    public VideoClip videoClip;
    private Scene scene;


    void Start()
    {

#if UNITY_EDITOR || UNITY_IOS || UNITY_ANDROID
        videoPlayer.source = VideoSource.VideoClip;
        videoPlayer.clip = videoClip;
#endif
        scene = SceneManager.GetActiveScene();
        videoPlayer.loopPointReached += LoadNextScene;
        StartCoroutine(PlayVideo());       
    }

    private void OnEnable()
    {
        StartCoroutine(PlayVideo());
    }

    IEnumerator PlayVideo()
    {
        videoPlayer.Prepare();
        while (!videoPlayer.isPrepared)
        {
            yield return null;
        }
        rawImage.texture = videoPlayer.texture;
        videoPlayer.Play();
        audioSource.Play();
    }

    void LoadNextScene(VideoPlayer vp)
    {
        SceneManager.LoadScene(scene.buildIndex + 1);
    }
}
