﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;
using DG.Tweening;

public class TextControl : MonoBehaviour
{
    public AudioSource buttonClickAudioSource;
    public GameObject nextPageButton;
    public GameObject previousPageButton;
    public Sprite playButtonSprite;
    public Sprite nextButtonSprite;
    public float fadeDuration;
    public GameObject[] textObjects;
    
    private Scene scene;
    private int textIndex;
    private bool isClicked;

    private void Start()
    {
        isClicked = false;
        scene = SceneManager.GetActiveScene();
        textIndex = 0;
    }

    private void Update()
    {

        if (textIndex == 1)
        {
            previousPageButton.SetActive(true);
        }

        if (textIndex == 0)
        {
            previousPageButton.SetActive(false);
        }
    }

    public void GoNextTextPage()
    {
        StartCoroutine(GoNextTextCoroutine());
    }

    public void GoPreviousTextPage()
    {
        StartCoroutine(GoPreviousTextCoroutine());
    }

    IEnumerator GoNextTextCoroutine()
    {
        if (!isClicked)
        {
            isClicked = true;
           
            if(textIndex == 0)
            {
                buttonClickAudioSource.Play();
                FadeOut(textObjects[0], fadeDuration);
                yield return new WaitForSeconds(fadeDuration);
                FadeIn(textObjects[1], fadeDuration);
                textIndex++;
            }
            else if (textIndex + 1 < textObjects.Length)
            {
                buttonClickAudioSource.Play();
                FadeOut(textObjects[textIndex], fadeDuration);
                yield return new WaitForSeconds(fadeDuration);
                FadeIn(textObjects[textIndex + 1], fadeDuration);

                if (textIndex + 1 == textObjects.Length - 1)
                {
                    if (scene.buildIndex == 12)
                    {
                        nextPageButton.SetActive(false);
                    }
                    nextPageButton.GetComponent<Image>().sprite = playButtonSprite;
                }         
            
                textIndex++;
            }
            else if (textIndex + 1 == textObjects.Length)
            { 
                buttonClickAudioSource.Play();
                yield return new WaitForSeconds(buttonClickAudioSource.clip.length);
                SceneManager.LoadScene(scene.buildIndex + 1);
            }
            
            isClicked = false;
        }
        else
            yield return null;
    }

    IEnumerator GoPreviousTextCoroutine()
    {
        if (!isClicked)
        {
            isClicked = true;

            if (textIndex - 1 >= 0)
            {
                if (scene.buildIndex == 12)
                {
                    nextPageButton.SetActive(true);
                }
                nextPageButton.GetComponent<Image>().sprite = nextButtonSprite;
                buttonClickAudioSource.Play();
                FadeOut(textObjects[textIndex], fadeDuration);
                yield return new WaitForSeconds(fadeDuration);
                FadeIn(textObjects[textIndex - 1], fadeDuration);
                textIndex--;
            }
            else if (textIndex - 1 < 0)
            {
                previousPageButton.SetActive(false);
                textIndex = 0;
            }

            isClicked = false;
        }   
        else
            yield return null;
    }

    public void FadeIn(GameObject gO, float duration)
    {
        Fade(gO, 0, 1, duration);
    }

    public void FadeOut(GameObject gO, float duration)
    {
        Fade(gO, 1, 0, duration);
    }

    public void Fade(GameObject gO, float startOpacity, float endOpacity, float duration)
    {
        Color startColor = gO.GetComponent<TMPro.TMP_Text>().color;
        startColor.a = startOpacity;
        gO.GetComponent<TMPro.TMP_Text>().color = startColor;

        var endColor = gO.GetComponent<TMPro.TMP_Text>().color;
        endColor.a = endOpacity;
        gO.GetComponent<TMPro.TMP_Text>().DOColor(endColor, duration).SetEase(Ease.OutQuart);
    }
}
